<div align="center">
	<img src="https://incubatech.com.br/static/66605966a04d5b9ef38feb2c196056eb/f1913/logo-incubatech.png" with="600"/>
	<h1>IncubaTech - Projeto 02</h1>
</div>

![Badge](https://img.shields.io/badge/Version-1.0.0-%23542F61?style=for-the-badge&logo=appveyor)
![Badge](https://img.shields.io/badge/Laravel-v8.65-%233570B2?style=for-the-badge&logo=laravel)
![Badge](https://img.shields.io/badge/PHP-v^7.4-%2300B98E?style=for-the-badge&logo=php)
![Badge](https://img.shields.io/badge/Composer-v2.1.9-%2300B98E?style=for-the-badge&logo=appveyor)

## 🎯 Desafio proposto:

Para o projeto, queremos que você faça um sistema de gerenciamento, que pode
ser de produtos, clientes, entregas, vendas, quadrinhos, você escolhe! Mas apesar de
deixarmos livre essa escolha, temos alguns requisitos, como:
- Deverá ser uma API RESTful;
- Em PHP, utilizando Laravel;
- Deverá, de alguma, manipular e armazenar endereços e consumir alguma API
para busca dos mesmos através do CEP (como esta, por exemplo:
https://github.com/filipedeschamps/BrasilAPI);
- Os endereços podem estar vinculados a um cliente, uma entrega, uma venda, até
mesmo a um produto indicando onde ele está e assim por diante, mas nunca
soltos sem relação alguma;
- Fazer versionamento com Git em alguma plataforma de sua escolha, como
GitHub, GitLab, BitBucket, entre outras...
- O fluxo da aplicação deve estar completo.

## 🛠 Ferramentas

-   [Laravel](https://laravel.com/docs/8.x)
-   [Insominia](https://insomnia.rest)
    -   [Import configuração de requisições](requisicoes.json)

## 📦 Requisitos para rodar o sistema

-   [PHP 7.4 ou superior](https://www.php.net/downloads)
    -   PHP7.4-curl
    -   PHP7.4-xml
    -   PHP7.4-mbstring
-   [Composer](https://getcomposer.org/download/)


## 💻 Padronização de código

-   [Eslint](https://eslint.org/)
-   [Prettier](https://prettier.io/)
-   [EditorConfig](https://editorconfig.org/)


## 🚀 Executando o projeto
### 1. Abra o terminal e clone o projeto para sua máquina
```bash
https://gitlab.com/gabrielopfaria/incubatech.git
```
### 2. Entre na pasta do projeto
```bash
cd incubatech
```
### 3. Baixe as dependências com composer
```bash
composer install
```
### 4. Copie o arquivo .env.example e renomeie para .env
```bash
cp .env.exemple .env
```
### 5. Crie uma tabela no banco de dados com o nome incubatech e altere no .env as configurações conforme seu banco
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=incubatech //altere aqui conforme seu banco de dados
DB_USERNAME=root //user name do seu banco de dados
DB_PASSWORD= //password para acessar seu banco de dados
```

#### Caso não tenha um banco de dados você pode gerar um container usando esse arquivo
-   [Docker-composer](docker-compose.yml)
```bash
docker-composer up -d
```

### 6. Gere uma chave de criptografia para o código
```bash
php artisan key:generate
```
### 7. Rode as migrations para gerar as tabelas no banco
```bash
php artisan migrate
```
### 8. Inicie o Server
```bash
php artisan serve
```
## 🎯 Rotas para o sistema
<span>_Por padrão o laravel cria servidor na porta :8000 | todas as requisições devem ser via application/json_</span>

> ### Para acessar as rotas de cliente, você deve estar autenticado no sistema, primeiro acesse a rota de register para criar um usuário e gerar seu token
```bash
http://localhost:8000/api/register
```
Parâmentros usados para criar usuário
Parâmetros   | Tipo
:--------- | :------
name | string - required
email | string - required
password | string - required
password_confirmation | string - required

```bash
{
  "name": "incubatech",
  "email": "user@incubatech.com.br",
  "password": "123456",
  "password_confirmation": "123456"
}
```
Após criar o usuário guarde o token para autenticação
```bash
{
  "user": {
    "name": "incubatech",
    "email": "user@incubatech.com.br",
    "updated_at": "2021-11-05T01:01:33.000000Z",
    "created_at": "2021-11-05T01:01:33.000000Z",
    "id": 2
  },
  "token": "2|p7m1VyxksQbbW4dQ8tddY7AYTbvMvQ03Nf8jgClc"
}
```

## _Ative o token Bearer e use o token gerado ao criar usuário_
```bash
Token: 2|p7m1VyxksQbbW4dQ8tddY7AYTbvMvQ03Nf8jgClc
```

> Rota para realizar login (Usar método <b>POST</b> na requisição)
```bash
http://localhost:8000/api/login
```
Parâmentros usados para logar
Parâmetros   | Tipo
:--------- | :------
email | string - required
password | string - required

> Listar todos os clientes (Usar método <b>GET</b> na requisição)
```bash
http://localhost:8000/api/clientes
```

> Cadastrar Cliente (Usar método <b>POST</b> na requisição)
```bash
http://localhost:8000/api/clientes
```
Parâmentros usados para cadastro de cliente
Parâmetros   | Tipo
:---------: | :------
nome | string - required
cpf | string - required - unique - max:14
telefone | string - required
cep | string - required max:9
numero | string - nullable

> Excluir Cliente (Usar método <b>DELETE</b> na requisição)
```bash
#Enviar o id do cliente
http://localhost:8000/api/clientes/{id}
```
> Buscar Cliente (Usar método <b>GET</b> na requisição)
```bash
#Enviar o id do cliente
http://localhost:8000/api/clientes/{id}
```

> Editar Cliente (Usar método <b>PUT</b> na requisição)
```bash
#Enviar o id do cliente
http://localhost:8000/api/clientes/{id}
```
Parâmentros usados para edição do cliente
Parâmetros   | Tipo
:---------: | :------
nome | string - required
cpf | string - required - unique - max:14
telefone | string - required
cep | string - required max:9
numero | string - nullable


## ✅ Features Desenvolvidas

-   [x] API RESTful
-   [x] Listar Clientes
-   [x] Cadastrar Clientes
-   [x] Editar Clientes
-   [x] BUscar Cliente
-   [x] Excluir Clientes
-   [x] API para buscar endereço via CEP e vincular a um cliente
-   [X] Autenticação de usuário para realizar tarefas na API


