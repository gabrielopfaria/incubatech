<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cliente = Cliente::all();
        if ($cliente->count() == 0) {
            return response()->json(['error' => 'Não existe cliente cadastrado.'], 404);
        }
        return response()->json(Cliente::all(), 200);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $regras = [
            'nome' => 'required',
            'cpf' => 'required|unique:clientes|max:14',
            'telefone' => 'required',
            'cep' => 'required|max:9'
        ];

        $validator = Validator::make($request->all(), $regras);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $data = $request->all();
        $endereco = Http::accept('application/json')->get("https://viacep.com.br/ws/$request->cep/json/");
        $data['cidade'] = $endereco['localidade'];
        $data['bairro'] = $endereco['bairro'];
        $data['estado'] = $endereco['uf'];
        $data['numero'] ? $data['numero'] : $data['numero'] = 'SN';
        $data['endereco'] = $endereco['logradouro'] . ' - Nº: ' . $data['numero'];

        return response()->json(Cliente::create($data), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $cliente = Cliente::find($id);
        if (!$cliente) {
            return response()->json(['error' => 'Cliente não cadastrado'], 404);
        }
        return response()->json($cliente, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, Cliente $cliente)
    public function update(Request $request, int $id)
    {
        $cliente = Cliente::find($id);
        if (!$cliente) {
            return response()->json(['error' => 'Cliente não cadastrado'], 404);
        }
        $endereco = Http::accept('application/json')->get("https://viacep.com.br/ws/{$request['cep']}/json/");

        $data = $request->all();
        $data['cidade'] = $endereco['localidade'];
        $data['bairro'] = $endereco['bairro'];
        $data['estado'] = $endereco['uf'];
        $data['numero'] ? $data['numero'] : $data['numero'] = 'SN';
        $data['endereco'] = $endereco['logradouro'] . ' - Nº: ' . $data['numero'];
        $cliente->update($data);

        return response()->json($cliente, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $cliente = Cliente::find($id);
        if (!$cliente) {
            return response()->json(['error' => 'Cliente não existe'], 404);
        }
        Cliente::destroy($cliente->id);
        return response()->json(['success' => 'Cliente apagado com sucesso!'], 200);
    }
}
